export TERM=xterm-256color
export CLICOLOR_FORCE=true
export RICHGO_FORCE_COLOR=1

default: setup-local

build: install
	@echo "### Building..."
	env GOOS=linux go build -o bin/service cmd/main.go

setup-local: install
	@go get -u golang.org/x/tools/...
	@go get -u golang.org/x/lint/golint
	@go get -u github.com/haya14busa/goverage
	@go get -u github.com/kyoh86/richgo
	@go get github.com/joho/godotenv/cmd/godotenv
	@curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(GOPATH)/bin v1.15.0
	@golangci-lint --version

install:
	@echo "### Downloading Dependencies..."
	@go mod verify

test: test-lint test-coverage

test-lint:
	@echo "### Run test-lint..."
	@golangci-lint run

test-coverage:
	@echo "### Run test-coverage..."
	@richgo test -failfast -coverprofile=coverage.out ./...
	@go tool cover -html=coverage.out -o coverage.html

clean:
	@go clean -modcache